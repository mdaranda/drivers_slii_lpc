/*
 * sapi_dht11.h
 *
 *  Created on: 7 jul. 2021
 *      Author: MARCO
 */
#include "chip.h"
#include "stdint.h"
#include "sapi_datatypes.h"

#ifndef SAPI_DHT11_H_
#define SAPI_DHT11_H_

#define dht11Config dht11Init

void dht11Init(int32_t gpio_port, int32_t gpio_pin );
bool_t dht11Read( float *phum, float *ptemp );

#endif /* SAPI_DHT11_H_ */
